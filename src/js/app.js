//Load fonts
WebFont.load({
  google: {
    families: ['Material+Icons', 'Lato:300,400,700']
  }
});
$(document).ready(function() {
  //Custom Popovers
  $('[data-toggle=custom-popover]').click(function(e) {
    e.preventDefault();
  });
  $('[data-toggle=custom-popover]').popover({
    html: true,
    title: '',
    content: function() {
      var content = $(this).attr('data-popover-content');
      return $(content).html()
    }
  })

  //Tooltips
  $('[data-toggle="tooltip"]').tooltip()
  $('[data-toggle="popover"]').popover()

  //Icons
  $('.verify-icon').addClass('bx bxs-shield bx-md');
  $('.time-icon').addClass('bx bxs-time bx-md');
  $('.plus-icon').addClass('bx bx-plus bx-md');
  $('.send-icon').addClass('bx bxs-send bx-md');
  $('.warning-icon').addClass('bx bxs-error bx-sm');
  $('.link-external-icon').addClass('bx bx-link-external bx-md');
  $('.whatsapp-icon').addClass('bx bxl-whatsapp bx-md');
  $('.verify-icon.icon-sm').removeClass('bx-md').addClass('bx-sm');
  $('.time-icon.icon-sm').removeClass('bx-md').addClass('bx-sm');

  //Listing mobile accordeon
  if ($(document).width() <= 991) {
    $('.listing-item .exchanger').click(function() {
      var listingData = $(this).parent().find('.list-item-data');
      var expanderData = $(this).find('.expand-listing');
      listingData.toggleClass('active');
      expanderData.toggleClass('active');
    });
  }

  //Timepicker
  $('input.timepicker').datetimepicker({
    format: 'LT'
  });

  //Enable schedule days
  $('.enable-schedule-day').change(function() {
    if ( $(this).is(':checked') ) {
      $(this).parent().parent().find('.sub-field').removeClass('disabled');
      $(this).parent().parent().find('.sub-field input').prop('disabled', false);
    } else {
      $(this).parent().parent().find('.sub-field').addClass('disabled');
      $(this).parent().parent().find('.sub-field input').prop('disabled', true);
    }
  });

  //Enable fields after currency selection
  $('select.currency-selector').change(function() {
    if ( $(this).val() && $(this).val() !== '' ) {
      $('.new-form .field.disabled').removeClass('disabled');
    }
  });

  //FIle input
  $('.fileinput').fileinput();
});
